
import argparse
from os import path
import matplotlib.pyplot as pyplot
import random
import numpy as np
from PIL import Image, ImageDraw, ImageFont

'''
cette methode est utilisee pour normaliser les couleurs rgb d'une image
'''
def normaliserRGB(img):
    if img.dtype == np.uint8:
        return img[:, :, :3] / 255.
    else:
        return img[:, :, :3].astype(np.float64)

'''
cette methode fait le "Spectrum Centralization"/la centralisation du spectrum (voir lien http://fourier.eng.hmc.edu/e161/lectures/fourier/node13.html) 
on l'utilise pour calculer le le coefficient d'energie 
'''
def centraliser(img, echelle=.06, clip=False):

    img = img.real.astype(np.float64)
    '''
    img.size=taille image*3 pour les 3 composantes de couleur /taille image =nbLigne*nbColonnes
       '''
    thres = img.size * echelle
    l = img.min()
    r = img.max()
    '''
       calcul la valeur minimale et maximale on utilisant une recherche dichotomique  
       '''
    while l + 1 <= r:
        m = (l + r) / 2.
        s = np.sum(img < m)
        if s < thres:
            l = m
        else:
            r = m
    low = l

    l = img.min()
    r = img.max()
    while l + 1 <= r:
        m = (l + r) / 2.
        s = np.sum(img > m)
        if s < thres:
            r = m
        else:
            l = m

    high = max(low + 1, r)
    img = (img - low) / (high - low)

    if clip:
        '''
           la methode clip   transforme les valeurs proches a 0 en 0/meme chose pour le 1
           '''
        img = np.clip(img, 0, 1)

    return img, low, high


    '''
    cette methode est utiliser pour generer les indices de mappage
    '''
def generationAleatoire(taille, password=None):
    r = np.arange(taille)
    '''
       genere aleatoirement des valeurs  dans l'intervalle [0, taille[ a partir d'un seed qui est le mot de passe (si le mot de passe est present).
       puisque la generation des nombres aleatoirement n'est pas vraiment aleatoire (suit un pattern sur le long terme) on utilise un seed qui est utilise' pour 
       generer les valeurs aleatoirement a partir d'une valeur de depart dans l'intervale ]nombreAleatoireMinimal,nombreAleatoireMaximal[
       '''
    if password:  # diff de None, ""
        random.seed(password)
        for i in range(taille, 0, -1):
            j = random.randint(0, i)
            r[i - 1], r[j] = r[j], r[i - 1]
    return r

'''
    cette methode genere les indices de mappage
    '''
def mappinggeneration(shape, secret=None):
    '''
        indicesHateurs va contenir des indices dans l'intervale [0,hauteurImageFiligrane]
                indicesLargeur va contenir des indices dans l'intervale [0,LargeurImageFiligrane]

        '''
    indicesHateurs, indiceslargeur = generationAleatoire(shape[0], secret), generationAleatoire(shape[1], secret)
    '''
          reshape((-1, 1))
          changer la dimensions colonnes pour qu'elle soit egale a 1
          le -1 signifie qu'on ne connait pas la dimension du nouveau nombre de lignes en avance (c'est le programme qui va s'en occuper)
          '''
    indicesHateurs = indicesHateurs.reshape((-1, 1))
    return indicesHateurs, indiceslargeur

def EncoderImage(image, filigrane, indicesMapping=None, margins=(1, 1), coeiffEnergie=None):
    imageNormalisee = normaliserRGB(image)
    filigraneNormalisee = normaliserRGB(filigrane)
    '''
             appliquer l'algorithme fft-2 sur l'image
             '''
    fa = np.fft.fft2(imageNormalisee, None, (0, 1))
    pb = np.zeros((imageNormalisee.shape[0] // 2 - margins[0] * 2, imageNormalisee.shape[1] - margins[1] * 2, 3))
    '''
                 preparer les valeurs de filigrane a ajouter dans le domaine de frequences en les mettant dans les bon indices
                 '''
    pb[:filigraneNormalisee.shape[0], :filigraneNormalisee.shape[1]] = filigraneNormalisee
    low = 0
    if coeiffEnergie is None:
        _, low, high = centraliser(fa)
        coeiffEnergie = (high - low)

    if indicesMapping is None:
        '''
                     generer les indices de mappage dans le cas d'absence
                     '''
        indicesHauteur, indiceslargeur = mappinggeneration(pb.shape)
    else:
        '''
                     recuperer les indices de mappage
                     '''
        indicesHauteur, indiceslargeur = indicesMapping[:2]


    '''
                 encoder les valeurs dans le domaine de frequences en utilisant les indices de largeur/hauteur du filigrane
                 '''
    fa[+margins[0] + indicesHauteur, +margins[1] + indiceslargeur] += pb * coeiffEnergie


    '''
                 ifft2 pour retourner vers le domaine spatial
                 '''
    imageEncodee_DomaineSpatial = np.fft.ifft2(fa, None, (0, 1))
    '''
                     ne garder que la partie reelle
                     '''
    imageEncodee_DomaineSpatial = imageEncodee_DomaineSpatial.real
    '''
                  arrondir les valeurs proche de 1 et de zero
                  '''
    imageEncodee_DomaineSpatial = np.clip(imageEncodee_DomaineSpatial, 0, 1)

    return imageEncodee_DomaineSpatial, fa



    '''
    cette methode ne fait que la egeneration de l'image filigrane a partir d'un texte en utilisant des librairies externes et fait appel a la methode EncoderImage() pour s'occuper du reste
    '''
def encoderTexte(oa, text, *args, **kwargs):
    font = ImageFont.truetype("consola.ttf", oa.shape[0] // 16)
    renderSize = font.getsize(text)
    padding = min(renderSize) * 2 // 10
    renderSize = (renderSize[0] + padding * 2, renderSize[1] + padding * 2)
    textImg = Image.new('RGB', renderSize, (0, 0, 0))
    draw = ImageDraw.Draw(textImg)
    draw.text((padding, padding), text, (255, 255, 255), font=font)
    ob = np.asarray(textImg)
    return EncoderImage(oa, ob, *args, **kwargs)


    '''
    decoder le message
    '''
def decoderImage(xa, xmap=None, margins=(1, 1), oa=None, full=False):
    na = normaliserRGB(xa)
    fa = np.fft.fft2(na, None, (0, 1))

    if xmap is None:
        xh = mappinggeneration((xa.shape[0] // 2 - margins[0] * 2, xa.shape[1] - margins[1] * 2))
    else:
        xh, xw = xmap[:2]

    if oa is not None:
        noa = normaliserRGB(oa)
        foa = np.fft.fft2(noa, None, (0, 1))
        fa -= foa

    if full:
        nb, _, _ = centraliser(fa, clip=True)
    else:
        nb, _, _ = centraliser(fa[+margins[0] + xh, +margins[1] + xw], clip=True)
    return nb




    '''
    afficher une image avec pyplot
    '''
def afficherImage(img, *args, **kwargs):
    img, _, _ = centraliser(img, clip=True)
    kwargs["interpolation"] = "nearest"
    if "title" in kwargs:
        pyplot.title(kwargs["title"])
        kwargs.pop("title")
    if len(img.shape) == 1:
        kwargs["cmap"] = "gray"
    pyplot.imshow(img, *args, **kwargs)


    '''
    asauvgarder une image
    '''
def sauvgarderImage(fn, img, *args, **kwargs):
    kwargs["dpi"] = 1

    if img.dtype != np.uint8:
        img, _, _ = centraliser(img, clip=True)
        img = (img * 255).round().astype(np.uint8)

    pyplot.imsave(fn, img, *args, **kwargs)


    '''
    gestion des arguments du lancement du programme+appel des methodes selon les arguments de la commande du lancement du programme
    '''
if __name__ == "__main__" or True:
    argparser = argparse.ArgumentParser(description=
                                        "steganographie")
    argparser.add_argument("input", metavar="fichier",
                           type=str, help="nom Image Originale.")
    argparser.add_argument("-o", "--output", dest="output",
                           type=str, help="Output filename.")
    argparser.add_argument("-s", "--password", dest="secret",
                           type=str, help="mot de passe pour generer les indices de mappage")
    argparser.add_argument("-i", "--image", dest="imagesign",
                           type=str, help="nom fichier image message.")
    argparser.add_argument("-t", "--text", dest="textsign",
                           type=str, help=" texte a encoder")
    argparser.add_argument("-a", "--alpha", dest="alpha",
                           type=float, help="coefficient d'energie")
    argparser.add_argument("-d", "--decoder", dest="decode",
                           type=str, help="nom image a decoder")
    argparser.add_argument("-v", dest="visual",
                           action="store_true", default=False, help="image d'affichage")
    args = argparser.parse_args()

    oa = pyplot.imread(args.input)
    margins = (oa.shape[0] // 7, oa.shape[1] // 7)
    margins = (1, 1)
    mapIndices = mappinggeneration((oa.shape[0] // 2 - margins[0] * 2, oa.shape[1] - margins[1] * 2), args.secret)
    if args.decode:
        xa = pyplot.imread(args.decode)
        xb = decoderImage(xa, mapIndices, margins, oa)
        if args.output is None:
            base, ext = path.splitext(args.decode)
            args.output = base + "-" + path.basename(args.input) + ext
        sauvgarderImage(args.output, xb)
        print("Image saved.")
        if args.visual:
            pyplot.figure()
            afficherImage(xb, title="Message Decodé")
    else:
        if args.imagesign:
            ob = pyplot.imread(args.imagesign)
            ea, fa = EncoderImage(oa, ob, mapIndices, margins, args.alpha)
            if args.output is None:
                base, ext = path.splitext(args.input)
                args.output = base + "+" + path.basename(args.imagesign) + ext
        elif args.textsign:
            ea, fa = encoderTexte(oa, args.textsign, mapIndices, margins, args.alpha)
            if args.output is None:
                base, ext = path.splitext(args.input)
                args.output = base + "_" + path.basename(args.textsign) + ext
        else:
            print("Erreur: veuillez indiquer une image ou un texte a encoder.")
            exit(2)

        sauvgarderImage(args.output, ea)
        print("Image sauvgardée.")
        if args.visual:
            xa = pyplot.imread(args.output)
            xb = decoderImage(xa, mapIndices, margins, oa)
            sauvgarderImage(args.output, xb)
            pyplot.figure()
            pyplot.subplot(221)
            afficherImage(ea, title="Image Encodée")
            pyplot.subplot(222)
            afficherImage(normaliserRGB(xa) - normaliserRGB(oa), title="delta")
            pyplot.subplot(223)
            afficherImage(fa, title="Domaine Frequentiel ")
            pyplot.subplot(224)
            afficherImage(xb, title="Message Decodé")
            pyplot.show()