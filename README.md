# Tatouage Images
(SAKHI MEHDI)

usage: Tatouage.py [-h] [-o OUTPUT] [-s SECRET] [-i IMAGESIGN] [-t TEXTSIGN]
                   [-a ALPHA] [-d DECODE] [-v]
                   fichier

steganographie

positional arguments:
  fichier               nom Image Originale.

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        Output filename.
  -s SECRET, --password SECRET
                        mot de passe pour generer les indices de mappage
  -i IMAGESIGN, --image IMAGESIGN
                        nom fichier image message.
  -t TEXTSIGN, --text TEXTSIGN
                        texte a encoder
  -a ALPHA, --alpha ALPHA
                        coefficient d'energie
  -d DECODE, --decoder DECODE
                        nom image a decoder
  -v                    image d'affichage
